/**
Viewer for a spiral drawing
 */
public class SpiralViewer
{
    public static void main(String[] args)
    {

        final int GRID_SIZE = 10;
        int endX = 100;
        int endY = 100;
        int startX;
        int startY;

        for (int n = 0; n < ...; n++)
        {
            startX = endX;
            startY = endY;

            endX = ...; 
            endY = ...;

            Line line = new Line(startX, startY, endX, endY);
            line.draw();
            //each iteration draws one segment of the line
        }
    }
}
