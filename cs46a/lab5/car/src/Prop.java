
import org.lgna.story.resources.PropResource;
import org.lgna.story.*;
import org.lgna.common.ForEachRunnable;
import org.lgna.common.DoTogether;
import org.lgna.common.ForEachTogether;

public class Prop extends SProp {

    public Prop(final PropResource resource) {
        super(resource);
    }
}
