
import org.lgna.story.MoveDirection;
import org.lgna.story.TurnDirection;
import org.lgna.story.resources.prop.ColaBottleResource;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cay
 */
public class Car extends RedRover 
{
    public void drive()
    {
    move(MoveDirection.FORWARD, 2);
    turn(TurnDirection.LEFT, Math.random());
    Prop bottle = new Prop(ColaBottleResource.COLA_BOTTLE);
    bottle.setPositionRelativeToVehicle(getPositionRelativeToVehicle());
    bottle.setVehicle(getVehicle());
    }
    
}
