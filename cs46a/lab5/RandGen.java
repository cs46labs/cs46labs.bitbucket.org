public class RandGen
{
   /** 
       Construct a generator for floating-point numbers between 0 
       (inclusive) and 1 (exclusive).
   */
   public RandGen()
   {
      limit = 1;
   }

   /** 
       Construct a generator for floating-point numbers between 0 
       (inclusive) and limit (exclusive).
       @param limit the upper bound
   */
   public RandGen(double limit)
   {
      this.limit = limit;
   }

   /** 
       Construct a generator for floating-point numbers between from 
       (inclusive) and to (exclusive).
       @param from the lower bound
       @param to the upper bound
   */
   public RandGen(double from, double to)
   {
      start = from;
      limit = to - from;
   }


   public double nextDouble()
   {
      return Math.random();
   }

   private double start;
   private double limit;
}
