import java.util.ArrayList;

public class StringPermuter
{
   public String scrambleSentence(String sentence)
   {
      String result = "";
      for (Word w : sentence.split("(\\s|\\p{Punct})+"))
      {
         String r = scramble(w);
         result = result + r + " ";
      }

      return result;
   }

   public String scramble(String word)
   {
      return word;
   }

   private String words;
}
