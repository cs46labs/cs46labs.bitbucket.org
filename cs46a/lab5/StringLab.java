import java.util.Scanner;

public class StringLab
{
   public static void main(String[] args)
   {
      Scanner in = new Scanner(System.in);

      System.out.print("Enter a string: ");
      String input = ...;

      String firstLetter = input.substring(0, 1);
      int lastIndex = ...;
      String lastLetter = input.substring(..., ...);
      int middleIndex = ...;
      String middleLetter = input.substring(middleIndex, middleIndex + 1);
      String beforeMiddleButNotFirst = input.substring(..., ...);
      String afterMiddleButNotLast = input.substring(..., ...);

      System.out.println(firstLetter + " " + lastLetter + ... ... ...);
   }
}
