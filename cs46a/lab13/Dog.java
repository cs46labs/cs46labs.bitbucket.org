import java.awt.Graphics2D;
import java.awt.Image;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Dog
{
   public Dog(int x, int y)
   {
      this.x = x;
      this.y = y;
      try 
      {
         image = ImageIO.read(new File("dog.png"));
      }
      catch (IOException ex)
      {
         System.out.println("Can't read image.");
      }
   }


   public void draw(Graphics2D g2)
   {
      g2.drawImage(image, x, y, null);
   }

   private int x;
   private int y;
   private Image image;
}
