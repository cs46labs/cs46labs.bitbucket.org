import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
   A grayscale picture.
*/
public class Picture extends JLabel
{ 
   public static int[][] getPixels(String filename)
   {
      try 
      {
	 BufferedImage image = ImageIO.read(new File(filename));    
         WritableRaster raster = image.getRaster();
	 int[][] pixels = new int[image.getWidth()][image.getHeight()];
         ColorModel model = image.getColorModel();
      
	 for (int i = 0; i < pixels.length; i++)
	    for (int j = 0; j < pixels[i].length; j++)
	    {
	       Color c = new Color(model.getRGB(raster.getDataElements(i, j, null)), true);
	       // Use NTSC/PAL algorithm to convert RGB to gray level
	       int gray = (int)(0.2989 * c.getRed() + 0.5866 * c.getGreen() + 0.1144 * c.getBlue());	       
	       pixels[i][j] = gray;
	    }
         return pixels;
      } 
      catch (IOException ex)
      {
         ex.printStackTrace();
	 return new int[0][0];
      }
   }

   public Picture(int[][] pixels)
   {
      BufferedImage image = new BufferedImage(pixels.length, pixels[0].length, BufferedImage.TYPE_INT_ARGB);
      WritableRaster raster = image.getRaster();
      ColorModel model = image.getColorModel();

      for (int i = 0; i < image.getWidth(); i++)
	 for (int j = 0; j < image.getHeight(); j++)
	 {
	    int gray = pixels[i][j];
	    if (gray < 0) gray = 0;
	    if (gray > 255) gray = 255;
	    Color c = new Color(gray, gray, gray);
	    int argb = c.getRGB();
	    Object colorData = model.getDataElements(argb, null);
	    raster.setDataElements(i, j, colorData);
	 }
      setIcon(new ImageIcon(image));
      setText(" ");
   }
}
