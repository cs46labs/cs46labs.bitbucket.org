<!DOCTYPE HTML >
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <title>Debugging</title>
  <link href="../../styles.css" rel="stylesheet" type="text/css">
</head>

<body>
<h1>Debugging Lab</h1>

    <p>Copyright © Cay S. Horstmann, Kathleen O’Brien 2009-2014 <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'><img alt='Creative Commons License' style='border-width:0' src='http://i.creativecommons.org/l/by-sa/4.0/88x31.png'/></a><br/>This work is licensed under a <a rel='license' href='http://creativecommons.org/licenses/by-sa/4.0/'>Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p> 


<p>When you hear the person sitting next to you saying &ldquo;I have a bug in
my program&rdquo;, you don't have to worry about roaches infesting the
computer. When we say <cite>bugs</cite> we really mean logical errors in our
program. The first bug, however, was a real live bug. Well, live at first. In
1947 at Harvard University, a moth was found in one of the components of the
Mark II computer, and was causing problems. </p>

<p><img alt="." src="firstbug.png" height="313" width="887"></p>

<p>In this lab you will deal with debugging. You will practice locating and
fixing some common types of bugs. The high point of the lab is a tour of the
debugger inside the BlueJ environment.</p>

<h2>A. Reading Stack Traces</h2>

<p>Make a BlueJ project from the files <a
href="WordAnalyzer.java">WordAnalyzer.java</a> and <a
href="WordAnalyzerTester.java">WordAnalyzerTester.java</a>. </p>

<p>Have a look at the <tt>WordAnalyzer</tt> class. A <tt>WordAnalyzer</tt> is
constructed with a string&mdash;the word to be analyzed. Right now, we only
care about the first (buggy) method: <tt>firstRepeatedCharacter</tt>. It
returns the first repeated character in a word, such as <tt>o</tt> in
<tt>r<u>oo</u>mmate</tt>. </p>

<p>The <tt>WordAnalyzerTester</tt> program simply tests the
<tt>WordAnalyzer</tt> class. </p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 1</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">Without actually
        running the program, predict its output. Assume that the
        <tt>firstRepeatedCharacter</tt> method works correctly. Scribe: Record your prediction.</td>
    </tr>
  </tbody>
</table>

<p></p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 2</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">Now run the
        program. Driver: What output do you get? (Include the correct outputs and the
        error message.)</td>
    </tr>
  </tbody>
</table>

<p>The program dies with an <strong>exception</strong> and prints a <i>stack trace</i>: </p>
<pre>Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 4<br>        at java.lang.String.charAt(String.java:558)<br>        at WordAnalyzer.firstRepeatedCharacter(WordAnalyzer.java:26)<br>        at WordAnalyzerTester.test(WordAnalyzerTester.java:14)<br>        at WordAnalyzerTester.main(WordAnalyzerTester.java:7)</pre>

<p>Have a look at the stack trace. The first complaint is about the method
<tt>charAt</tt> of the <tt>java.lang.String</tt> class. That's a library class.
It is extremely unlikely that a library class has a bug. It is far more likely
that your program called one of its methods   with bad parameters. </p>

<p>The next complaint is about line 26 of the <tt>firstRepeatedCharacter</tt>
method of the <tt>WordAnalyzer</tt> class. To find line 26 easily, turn on line numbers.</p>
<p class="note">Note: To display line numbers in BlueJ, click <em>Options</em>in the main menu. Select <em>Preferences</em> and then check the box for <em>Display line numbers</em></p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 3</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">Driver: Exactly what
        source code do you find in line 26 of <tt>WordAnalyzer</tt>? Past it into your report.</td>
    </tr>
  </tbody>
</table>


<p>Now look at the call to <tt>charAt</tt> in line 26.</p>
<pre>word.charAt(i + 1)<br><br></pre>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 4</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">In theory, there
        are two different exceptions that can be thrown in this call. Scribe: What are
        they? </td>
    </tr>
  </tbody>
</table>
<br>


<p>Of course, we know that <tt>word</tt> is not <tt>null</tt> in this case (So it can not be a NullPointerException), so
we know that the error must be an index out of bounds exception.  </p>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 5</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">(a) Scribe: Explain the
        nature of the bug, and how to fix it. <br>
        (b) Driver: What output did you get after
        you fixed the bug?</td>
    </tr>
  </tbody>
</table>
<br>


<h2>B. Running a Debugger</h2>

<p>Exceptions are useful to pinpoint drastic errors that kill your program. But
often, a sick program doesn't die. It limps along and computes the wrong
result. In order to find out what such a program does, students often sprinkle
their code with print statements like this one:<br>
</p>
<pre>System.out.println("Yoohoo! I got this far!");
System.out.println("i=" + i);</pre>

<table border="1" cellpadding="2" cellspacing="2" width="100%">
  <tbody>
    <tr>
      <td><img alt="." src="soapbox.gif" height="104" width="104"></td>
      <td>Print statements can be effective, but they can take a lot of your time. You put them in, you
        take them out, you put them back in, you comment them out, you remove
        the comments, and when it all works you take them out for good. Until
        the next bug appears.</td>
    </tr>
  </tbody>
</table>

<p>But there is a better way, especially as programs get bigger and more complex. Most development environments, including BlueJ
and Eclipse, have a <i>debugger</i>, a tool that lets you execute a program in
slow motion. You can observe which statements are executed and you can peek
inside variables. </p>

<p>Debuggers can be complex, but fortunately there are only three techniques
that you need to master:</p>
<ol>
  <li>Set breakpoint</li>
  <li>Single step</li>
  <li>Inspect variable</li>
</ol>
In this lab, we will use the debugger that is a part of BlueJ. 

<p>For this section, you need the <a
href="WordAnalyzerTester3.java"><tt>WordAnalyzerTester3</tt></a> class that
tests the buggy <tt>countRepeatedCharacters</tt> method. That method counts the
substrings consisting of repeated character in a word. For example, the word
<tt>mi<u>ss</u>i<u>ss</u>i<u>ppiii</u></tt> has a count of 4. (Yes, it is misspelled : ) )</p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 1</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1"
        colspan="1">Execute <tt>WordAnalyzerTester3</tt> in BlueJ. Driver: What output
        do you get?</td>
    </tr>
  </tbody>
</table>

<p>The <tt>countRepeatedCharacters</tt> method does the right thing for the
first two test cases, but it mysteriously fails on the
string <tt>"<u>aabb</u>cd<u>aaaabb</u>"</tt>. It should report 4 repetitions,
but it only reports 3.  </p>

<h3>Setting Breakpoints</h3>

<p>Unfortunately, the debugger cannot "go back", so you can't simply go to the
point of failure and backtrack. Instead, you first run your program at full
speed until it comes close the point of failure. Then you slow down and execute
small steps, watching what happens. </p>

<p>We know that the first two calls to the <tt>test</tt> method in
<tt>WordAnalyzerTester3</tt> give the correct results. It won't be too
interesting to debug them. Instead, let's go directly to the third call. We'll
set a <i>breakpoint</i> at that line, so that the debugger stops as soon as it
reaches the line.<br>
<br>
In BlueJ, open the WordAnalyzerTester3 class for editing. Click left of the
vertical line in the line containing the third call to <code>test</code>. A red
stop sign should appear, indicating the breakpoint.<br>
<br>
<img alt="." src="breakpoint.png"><br>
<br>
</p>
Now run the program. When it hits the first breakpoint, the program stops and
the current line is highlighted: 

<p><img alt="" src="breakpoint2.png" width="612" height="516"></p>

<p></p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 2</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">Launch the
        debugger as described. Driver: What output do you get in the terminal window?
        Scribe: Why do you get two lines of output and not three? </td>
    </tr>
  </tbody>
</table>
<br>


<h3>Single stepping</h3>

<p>When the debugger stops at a breakpoint, another window appears, with a row
of big buttons on the bottom:</p>

<p><img alt="" src="debugger.png" width="503" height="503"></p>
You use the <strong>Step</strong> and <strong>Step Into</strong> buttons to
step through your code in slow motion.<br>


<p></p>

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 3</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">Execute the
        "Step Into" command. Scribe: What happens? </td>
    </tr>
  </tbody>
</table>
<br>
Now you are inside the <tt>test</tt> method. The next call is <br>

<pre>WordAnalyzer wa = new WordAnalyzer(s);</pre>
That call is boring, and we do not want to step into the constructor. We'll use
the "Step" command instead. <br>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 4</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">Execute the
        "Step" command. Scribe: What happens? </td>
    </tr>
  </tbody>
</table>
<br>
Now you are at the line <br>

<pre>int result = wa.countRepeatedCharacters();</pre>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 5</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">Remember, we
        want to find out why we get the wrong repetition count for the third
        test case. Scribe: Should you execute "Step" or "Step Into" at this point?
      Why? </td>
    </tr>
  </tbody>
</table>

<h3>Inspecting Variables</h3>

<p>Execute "Step Into" a couple of times. You should get into the lines<br>
</p>
<pre>int c = 0;<br></pre>

<p>and</p>
<pre>for (int i = 1; i &lt; word.length() - 1; i++)</pre>

<p>Look inside the <strong>Local v</strong><strong>ariables</strong> window in
the bottom right corner. <br>
<br>
<img alt="" src="variables.png" width="355" height="137"><br>
</p>
<br>
Now keep executing the <strong>Step</strong> command. Watch what happens to the
<tt>c</tt> and <tt>i</tt> values. You'll see <tt>i</tt> increase each time the
loop is executed. <br>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 6</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">The value of
        <tt>c</tt> increases three times. Scribe: What are the values for <tt>i</tt> at
        each increase? </td>
    </tr>
  </tbody>
</table>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 7</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">Look at the
        value of the <tt>word</tt> instance variable. Scribe: What is special about the
        three positions at which <tt>c</tt> increases?  </td>
    </tr>
  </tbody>
</table>
<br>
We will fix the bug in the next section. For now, click the <b>Continue</b>
button. Your program runs again at full speed, until it hits the next
breakpoint or until it terminates. <br>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 8</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><span class="question">Scribe: What</span> happens
        when you click the "Continue" button? </td>
    </tr>
  </tbody>
</table>
<br>
You now know how to use the debugger. You have learned how to set breakpoints,
how to single-step, and how to view variables. <br>


<h3>Fixing the bug</h3>
The <tt>countRepeatedCharacters</tt> method looks for character sequences of
the form <tt>x<b>y</b>y</tt>, that is, a character followed by the same
character and preceded by a different one. That is the <i>start</i> of a group.
Note that there are two conditions. The condition<br>

<pre><tt>if (word.charAt(i) == word.charAt(i + 1)) </tt></pre>
tests for <tt><b>y</b>y</tt>, that is, a character that is followed by another
one just like it. But if we have a sequence <tt>yyyy</tt>, we only want it to
count once. That's why we want to make sure that the preceding character is
different:<br>

<pre>if (word.charAt(i - 1) != word.charAt(i))</pre>
This logic works almost perfectly: it finds three group starts:
<tt>aa<b>b</b>bcd<b>a</b>aaa<b>b</b>b<br>
<br>
</tt> 

<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step 9</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1" class="question"><span >Scribe: Why</span> doesn't the
        method find the start of the first (<tt>aa</tt>) group? </td>
    </tr>
  </tbody>
</table>
<br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step
        10</b><br>
      </td>
    </tr>
    <tr>
      <td colspan="1" rowspan="1" class="question" style="vertical-align: top;">Scribe: Why can't you
        simply fix the problem by letting <tt>i</tt> start at 0 in the
        <tt>for</tt> loop? </td>
    </tr>
  </tbody>
</table>
 <br>


<table
style="text-align: left; width: 100%; background-color: rgb(255, 255, 204);"
border="1" cellpadding="2" cellspacing="2">
  <tbody>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1"><b>Step
        11</b><br>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: top;" rowspan="1" colspan="1">Go ahead and fix
        the bug. (a) What is the code of your  <tt>countRepeatedCharacters</tt>
        method now? (b) Run the <tt>WordAnalyzerTester3</tt> method again. When you get to the breakpoint, click continue to run full speed to the end. What
        is the output now? </td>
    </tr>
  </tbody>
</table>
<br>


<table border="1" cellpadding="2" cellspacing="2" width="100%">
  <tbody>
    <tr>
      <td><img alt="." src="soapbox.gif" height="104" width="104"></td>
      <td>Is the program now free from bugs? That is not a question the
        debugger can answer. As the famous computer scientist <a
        href="http://en.wikipedia.org/wiki/Edsger_W._Dijkstra"
        title="Dijkstra">Edsger Dijkstra</a> pointed out: "<i>Program testing
        can be used to show the presence of bugs, but never to show their
        absence!</i>" As you have seen in this lab, testing and debugging is a
        laborious activity. In your computer science education, it pays to pay
        special attention to the tools and techniques that ensure correctness
        without testing. </td>
    </tr>
  </tbody>
</table>

<p>Driver: Run the tester again. When you get to the breakpoint, take  a screen shot of the Bluej debugger plus the editor window showing the breakpoint. Upload it into Canvas along with your report. 
If you do not know how to do this, you need to ask. This is an important skill (and you will need to do it on the upcoming exam.)</p>
<h2>&nbsp;</h2>
<h2>C. Debugging your Homework</h2>

<p>Either use <a href="vowel_finder.zip">this Bluej projec</a>t or try the process on a homework with a failing test case. </p>
<p>The given Bluej project has two bugs. Can you find them? It is suppose to return a string containing the vowels in the word. The first bug is an exception. After you fix that problem, run it again. Look at the output. Ask yourself  "Why are some results blank? Why are some printing out one vowel repeatedly?"</p>

<p>1. Describe what you do to get close to the point where the bug happens.</p>

<p>2. Describe the values of all variables just before the bug happens.</p>

<p>3. Single-step over the place where the bug happens. Did it happen?</p>

<p>4. When you look at the values of the variables, can you tell why the bug
happened? Which variable wasn't set to the right value? </p>

<p>5. Did you get enough information to fix the bug? </p>

<h2>D. Selfcheck Quiz20 </h2>
<p>Complete Quiz20 if you want to leave the lab early. </p>
<p>Ask the lab instructor if you have questions. </p>
<!--
<h2>D. Installing the virtual machine</h2>
<p>Time to take your first step in to the big league. Everyone working in computer science needs to know how to use a virtual machine. This week you will install Virtual Box and a Lubuntu virtal machine.</p>
<p>You will use the Virtual Machine in a future lab to learn some UINIX commands - something else everyone in Computer Science needs to know.</p>
<p>Go to <a href="../../vm/vm.html">virtual machine lab</a> and follow the instructions.</p>
<p>Both partners need to install the virtual machine.</p>
<p>&nbsp;</p>
-->

</body>
</html>
