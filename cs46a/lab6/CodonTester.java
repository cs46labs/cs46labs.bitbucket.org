public class CodonTester
{
   public static void main(String[] args)
   {
      String bases = "UUU";
      System.out.println(bases + " encodes Phe: " 
         + new Codon(bases).isPhe());
      System.out.println("Expected: true");
      bases = "UUA";
      System.out.println(bases + " encodes Phe: " 
         + new Codon(bases).isPhe());
      System.out.println("Expected: false");
      bases = "UUA";
      System.out.println(bases + " encodes Leu: " 
         + new Codon(bases).isLeu());
      System.out.println("Expected: true");
      bases = "UUU";
      System.out.println(bases + " encodes Leu: " 
         + new Codon(bases).isLeu());
      System.out.println("Expected: false");
      bases = "CUU";
      System.out.println(bases + " encodes Leu: " 
         + new Codon(bases).isLeu());
      System.out.println("Expected: true");
      bases = "AUU";
      System.out.println(bases + " encodes Leu: " 
         + new Codon(bases).isLeu());
      System.out.println("Expected: false");
      bases = "UUC";
      System.out.println(bases + " encodes Leu: " 
         + new Codon(bases).isLeu());
      System.out.println("Expected: false");
      bases = "AUU";
      System.out.println(bases + " encodes Ile: " 
         + new Codon(bases).isIle());
      System.out.println("Expected: true");
      bases = "AUA";
      System.out.println(bases + " encodes Ile: " 
         + new Codon(bases).isIle());
      System.out.println("Expected: true");
      bases = "AUG";
      System.out.println(bases + " encodes Ile: " 
         + new Codon(bases).isIle());
      System.out.println("Expected: false");
   }
}
