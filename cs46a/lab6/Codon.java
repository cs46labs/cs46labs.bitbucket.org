public class Codon
{
   public Codon(String bases)
   {
      this.bases = bases;
   }

   /**
      Tests whether this Codon encodes Phe
      @return true if this Codon encodes Phe
   */
   public boolean isPhe()
   {
      return bases.startsWith("UU") && "UC".contains(bases.substring(2));
   }

   /**
      Tests whether this Codon encodes Leu
      @return true if this Codon encodes Leu
   */
   public boolean isLeu()
   {
      return false;
   }

   /**
      Tests whether this Codon encodes Leu
      @return true if this Codon encodes Ile
   */
   public boolean isIle()
   {
      return false;
   }

   private String bases;
}
