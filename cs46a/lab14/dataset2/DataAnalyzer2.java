import java.util.Scanner;

/**
   This program computes the average and maximum of a set
   of input values.
*/
public class DataAnalyzer2
{  
   public static void main(String[] args)
   {  
      Scanner in = new Scanner(System.in);
      double sum = 0;
      double count = 0;
      double maximum = 0;
      boolean done = false;
      while (!done)
      {  
         System.out.print("Enter value, Q to quit: ");
         String input = in.next(); 
         if (input.equalsIgnoreCase("Q"))
            done = true;
         else
         {  
            double x = Double.parseDouble(input);
            sum = sum + x;
            if (count == 0 || maximum < x) maximum = x;
            count++;
         }
      }

      double average;
      if (count == 0) average = 0;
      else average = sum / count;

      System.out.println("Average = " + average);
      System.out.println("Maximum = " + maximum);
   }
}
