import java.util.*;

public class Test
{
   public static void main(String[] args)
   {
      Scanner in = new Scanner(System.in);
      String line = in.nextLine();
      if (line.startsWith(args[0]))
         System.out.println("pass");
      else
         System.out.printf("fail: %x\n" , Integer.valueOf(line.charAt(0)));
   }
}
